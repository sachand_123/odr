#pragma once

#define ODR_CLIENT_RAND_MAX_FILENAME 90

typedef struct odr_client_t
{
    char file_name[ODR_CLIENT_RAND_MAX_FILENAME];
    uint16_t port;

    struct odr_client_t *next;
} odr_client;

typedef odr_client *odr_client_list;
