#include "app_settings.h"

#define LOG_TAG "EthFrameProc"

/**
 * Creates an ethernet frame header.
 * 
 * @param frame     Ethernet frame received over raw sock
 * 
 * @return Pointer to freshly alloc-ed memory that contains frame header.
 *         Caller's responsibility to free the memory
 * */
ethernet_frame_header *
extract_ethernet_frame_header(char *frame)
{
    ethernet_frame_header *h = calloc(1, sizeof(ethernet_frame_header));
    memcpy(h->destination_hardware_address, frame, HW_ADDRLEN);
    frame += HW_ADDRLEN;
    memcpy(h->source_hardware_address, frame, HW_ADDRLEN);
    frame += HW_ADDRLEN;

    sscanf(frame, "%u", h->ethertype);
    h->ethertype = ntohl(h->ethertype);

    return h;
}

/**
 * Whether this node is the source
 * */
boolean
is_created_by_self (odr_router *router, msg_header *header)
{
    return (0 == strcmp(router->self_canonical_ip, header->source_id))
            ? TRUE : FALSE;
}

/**
 * Whether this node is the destination
 * */
boolean
is_destined_to_self (odr_router *router, msg_header *header)
{
    return (0 == strcmp(router->self_canonical_ip, header->destination_id))
            ? TRUE : FALSE;
}

/**
 * To be invoked when a message is received by ODR. It extracts important
 * information from the message like route to source.
 * 
 * @param router            Router object
 * @param eth_header        Ethernet header object
 * @param m_header          msg-header object
 * @param interface_index   Interface index in which this message arrived.
 * 
 * @return TRUE if information in this message is new and better, FALSE
 *         otherwise.
 * */
boolean
extract_base_info (odr_router *router, ethernet_frame_header *eth_header,
        msg_header *header, interface interface_index)
{
    routing_table_entry rte;
    memset(&rte, 0, sizeof(routing_table_entry));
    memcpy(rte.destination, header->source_id, INET_ADDRSTRLEN);
    memcpy(rte.next_hop, eth_header->source_hardware_address, HW_ADDRLEN);
    rte.outgoing_if = interface_index;
    rte.hop_count = header->hop_count;
    
    return update_table(&(router->rt_table), &rte);
}

/**
 * Relays a broadcast REQ.
 * 
 * @param router            Router object
 * @param m_header          msg-header object
 * @param interface_index   Interface index in which this message arrived.
 * @param better_route      Whether the received message gave better info to
 *                          the routing table
 * @param rte               Routing table entry for the message's destination
 * 
 * @return Number of interfaces on which broadcast was done
 * */
int
relay_broadcast (odr_router *router, msg_header *m_header, interface interface_index,
        boolean better_route, routing_table_entry *rte)
{
    endpoint_list list = router->interface_hub;

    if (TRUE == have_only_one_interface(list) || (
            FALSE == should_relay_broadcast(rte, m_header->broadcast_id) &&
            FALSE == better_route))
    {
        return 0;
    }
    
    struct iovec msg_iov[2];
    msg_iov[0].iov_base = m_header;
    msg_iov[0].iov_len = HEADER_SIZE;
    msg_iov[1].iov_base = m_header + HEADER_SIZE;
    msg_iov[1].iov_len = m_header->payload_length;
    
    return socket_l2_flood_msg(list, find_endpoint(list, interface_index),
            msg_iov);
}

/**
 * Processes an incoming message with REQ bit set
 * 
 * @param router            Router object
 * @param m_header          msg-header object
 * */
void
process_incoming_dat (odr_router *router, msg_header *m_header)
{
    msg_iovec msg = clone_msg(m_header, m_header + sizeof(msg_header),
            m_header->payload_length);
    
    if (TRUE == is_destined_to_self(router, m_header))
    {
        odr_route_data(router, msg);
    }
    else
    {
        odr_route_msg(router, msg);
    }
    delete_msg(msg);
}

/**
 * Processes an incoming message with REP bit set
 * 
 * @param router            Router object
 * @param eth_header        Ethernet header object
 * @param m_header          msg-header object
 * @param interface_index   Interface index in which this message arrived.
 * @param better_route      Whether the received message gave better info to
 *                          the routing table
 * @param rte               Routing table entry for the message's destination
 * */
void
process_incoming_rep (odr_router *router, ethernet_frame_header *eth_header,
        msg_header *m_header, interface interface_index, boolean better_route,
        routing_table_entry *rte)
{
    endpoint_list list = router->interface_hub;
    endpoint *e = find_endpoint(list, interface_index);
    msg_iovec msg = clone_msg(m_header, NULL, 0);
    
    if (TRUE == is_destined_to_self(router, m_header))
    {
        odr_car *car;
        for (car = rte->parking_lot; NULL != car; car = car->next)
        {
            socket_l2_send_unicast(e, car->msg, eth_header->source_hardware_address);
        }
    }
    else
    {
        if (FALSE == is_stale(rte))
        {
            if (TRUE == better_route)
            {
                m_header->flags |= MSG_HEADER_FLAG_REQ;
                socket_l2_flood_msg(list, e, msg);
            }

            m_header->hop_count += 1;
            socket_l2_send_unicast(e, msg, rte->next_hop);
        }
        else
        {
            odr_route_msg(router, msg);
        }
    }
    delete_msg(msg);
}

/**
 * Processes an incoming message with REQ bit set
 * 
 * @param router            Router object
 * @param eth_header        Ethernet header object
 * @param m_header          msg-header object
 * @param interface_index   Interface index in which this message arrived.
 * @param better_route      Whether the received message gave better info to
 *                          the routing table
 * @param rte               Routing table entry for the message's destination
 * */
void
process_incoming_req (odr_router *router, ethernet_frame_header *eth_header,
        msg_header *m_header, interface interface_index, boolean better_route,
        routing_table_entry *rte)
{
    m_header->hop_count += 1;

    endpoint_list list = router->interface_hub;
    endpoint *e = find_endpoint(list, interface_index);
    msg_iovec msg = clone_msg(m_header, NULL, 0);
    boolean rep_sent = is_flag_set(m_header->flags, MSG_HEADER_FLAG_REP);
    
    if (FALSE == is_stale(rte))
    {
        if (TRUE == better_route)
        {
            m_header->flags |= MSG_HEADER_FLAG_REP;
            socket_l2_flood_msg(list, e, msg);
        }

        if (FALSE == rep_sent)
        {
            m_header->hop_count = rte->hop_count;
            swap_roles(m_header);
            m_header->flags &= ~(MSG_HEADER_FLAG_REQ);
            m_header->flags |= MSG_HEADER_FLAG_REP;

            socket_l2_send_unicast(e, msg, eth_header->source_hardware_address);
        }
    }
    else
    {
        relay_broadcast(router, m_header, interface_index, better_route, rte);
    }
    delete_msg(msg);
}

/**
 * --------------------- THE CORE --------------------- 
 * Reads an incoming ethernet frame and sends appropriate reply for it.
 * Note that reading a frame and replying for it is an atomic operation
 * iff the message can be immediately serviced.
 * 
 * @param router            Router object
 * @param frame             Received ethernet frame
 * @param interface_index   Interface index in which this message arrived.
 * */
void
process_incoming_ethernet_frame (odr_router *router, char *frame,
        interface interface_index)
{
    ethernet_frame_header *eth_header = extract_ethernet_frame_header(frame);
    frame += ETH_FRAME_HEADER_SIZE;
    
    msg_header *m_header = (msg_header *)frame;
    frame += HEADER_SIZE;

    if (TRUE == is_flag_set(m_header->flags, MSG_HEADER_FLAG_FRC))
    {
        purge_entry(&(router->rt_table), m_header->destination_id);
    }

    // Extract useful info from the msg_header, if any
    boolean better_route = extract_base_info(router, eth_header,
            m_header, interface_index);
    routing_table_entry *rte = query_table(&(router->rt_table),
            m_header->destination_id);

    if (TRUE == is_flag_set(m_header->flags, MSG_HEADER_FLAG_REQ) &&
            FALSE == is_created_by_self(router, m_header))
    {
        process_incoming_req(router, eth_header, m_header, interface_index,
                better_route, rte);
    }
    else if (TRUE == is_flag_set(m_header->flags, MSG_HEADER_FLAG_REP))
    {
        process_incoming_rep(router, eth_header, m_header, interface_index,
                better_route, rte);
    }
    else if (TRUE == is_flag_set(m_header->flags, MSG_HEADER_FLAG_DAT))
    {
        process_incoming_dat(router, m_header);
    }
    
    free(eth_header);
}
