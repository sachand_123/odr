#include "app_settings.h"

extern float g_staleness;

boolean
is_stale (routing_table_entry *e)
{
    if (NULL == e)
    {
        return TRUE;
    }

    long time_elapsed = (long)get_current_system_time_millis() -
            e->timestamp;
    return (time_elapsed > g_staleness) ? TRUE : FALSE;
}

boolean
is_better (routing_table_entry *e1, routing_table_entry *e2)
{
    if (TRUE == is_stale(e2))
    {
        return TRUE;
    }

    if (e1->hop_count < e2->hop_count ||
            (e1->hop_count == e2->hop_count &&
            0 != memcmp(e1->next_hop, e2->next_hop, HW_ADDRLEN)))
    {
        return TRUE;
    }

    return FALSE;
}

routing_table_entry *
query_table (routing_table *t, char *destination)
{
    routing_table_entry *e = find_routing_table_entry(*t, destination);
    if (TRUE == is_stale(e))
    {
        purge_entry(t, destination);
        e = NULL;
    }

    return e;
}

boolean
update_table (routing_table *t, routing_table_entry *e)
{
    routing_table_entry *entry = find_routing_table_entry(*t, e->destination);
    if (TRUE == is_better(e, entry))
    {
        if (NULL == entry)
        {
            entry = create_routing_table_entry(); 
        }
        memcpy(entry, e, sizeof(routing_table_entry));
        entry->timestamp = get_current_system_time_millis();
        insert_in_routing_table(t, entry);
        return TRUE;
    }
    
    return FALSE;
}

boolean
should_relay_broadcast(routing_table_entry *e, uint32_t broadcast_id)
{
    relaying_history *h = &(e->relaying_broadcasts);
    boolean relay = TRUE;
    
    if (NULL != h->history)
    {
        int i;
        for (i = 0; i < h->history_size; ++i)
        {
            if (h->history[i] == broadcast_id)
            {
                relay = FALSE;
                break;
            }
        }
    }
    else
    {
        h->history = calloc(10, sizeof(uint32_t));
        h->history_size = 10;
        h->next_slot = 0;
    }
    
    if (TRUE == relay)
    {
        if (h->next_slot == h->history_size)
        {
            uint32_t *t = calloc(10 + h->history_size, sizeof(uint32_t));
            memcpy(t, h->history, h->history_size * sizeof(uint32_t));
            free(h->history);
            h->history = t;
            h->history_size += 10;
        }
        
        h->history[h->next_slot] = broadcast_id;
        h->next_slot = h->history_size;
        
        int i;
        for (i = 0; i < h->history_size; ++i)
        {
            if (0 == h->history[i])
            {
                h->next_slot = i;
                break;
            }
        }
    }

    return relay;
}
