#include "app_settings.h"

#define LOG_TAG "ODR_Frontend"

/**
 * Routes a message inside this system. Finds a right ODR client for
 * given msg using its destination port.
 * 
 * @param router        ODR router object
 * @param msg           Message to route in the system
 * 
 * @return Whether the msg was successfully routed. By this we mean the
 *         client's buffer was filled with msg-data.
 * */
boolean
route_to_self (odr_router *router, msg_iovec msg)
{
    msg_header *header = msg[0].iov_base;
    odr_client *oc = find_odr_client(router->client_list, header->destination_port);
    if (NULL != oc)
    {
        if (0 > ipc_send_msg_full(router->ipc_sock, oc->file_name, msg))
        {
            LOGI("Received message for broken ODR client.");
            return FALSE;
        }
    }

    return TRUE;
}

/**
 * Routes a data message in the system. Calls odr_route_msg to find a
 * route if not destined to self.
 * */
void
odr_route_data (odr_router *router, msg_iovec msg)
{
    msg_header *header = msg[0].iov_base;
    if (FALSE == is_flag_set(header->flags, MSG_HEADER_FLAG_DAT)) { return; }

    if (TRUE == is_destined_to_self(router, header))
    {
        // we can add ERR here if route to self fails
        route_to_self(router, msg);
    }
    else
    {
        odr_route_msg(router, msg);
    }
}

static void
__do_process(odr_router *router, struct msghdr *msghdr)
{
    msg_iovec msg = msghdr->msg_iov;
    msg_header *header = msg[0].iov_base;

    struct sockaddr_un *sun = msghdr->msg_name;
    LOGV("suname: %s", sun->sun_path);
    header->source_port = register_odr_client(&(router->client_list),
            sun->sun_path, header->source_port);
    memcpy(header->source_id, router->self_canonical_ip, INET_ADDRSTRLEN);
    
    odr_route_data(router, msg);
    delete_msg(msg);
}

/**
 * Core of the frontend. Listens on IPC file for DAT requests.
 * */
static void *
do_process (void *args)
{
    struct msghdr *msg = calloc(1, sizeof(struct msghdr));
    odr_router *router = args;
    
    LOGV("router %x %d", router, router->ipc_sock);
    while (FALSE == router->stopping)
    {
        memset(msg, 0, sizeof(struct msghdr));
        int bytes_read = socket_recv_msg_full(router->ipc_sock, msg,
                ODR_IPC_READ_TIMEOUT_MS);

        if (0 > bytes_read)
        {
            LOGE("Error during IPC read: %s", errno_string());
            break;
        }
        else if (0 < bytes_read)
        {
            __do_process(router, msg);
        }
    }

    router->stopping = TRUE;
    free(msg);
    LOGV("backend closing");
    return NULL;
}

/**
 * Starts the frontend of router
 * 
 * @param sock      Internal system socket of router that the service
 *                  listens on
 * @param router    Router object
 * 
 * @return Slave thread that handles frountend
 * */
pthread_t
start_odr_service_frontend (odr_router *router)
{
    LOGV("router1 %x %d", router, router->ipc_sock);
    pthread_t slave;
    pthread_create(&slave, NULL, &do_process, router);
    
    return slave;
}
