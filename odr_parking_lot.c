#include "app_settings.h"

#define LOG_TAG "ODR_ParkingLot"
void
destroy_odr_car (odr_car *e)
{
    if (NULL == e) { return; }

    delete_msg(e->msg);
}

void
destroy_odr_parking_lot (odr_parking_lot l)
{
    odr_car *e, *s;
    for (e = l; NULL != e; e = s)
    {
        s = e->next;
        destroy_odr_car(e);
    }
}

odr_car *
create_odr_car (msg_iovec msg)
{
    odr_car *t = (odr_car *) calloc(1, sizeof(odr_car));
    msg_header *header = msg[0].iov_base;
    t->msg = clone_msg(header, msg[1].iov_base, header->payload_length);
}

void
insert_in_odr_parking_lot (odr_parking_lot *t, odr_car *e)
{
    odr_car *p = *t;
    *t = e;
    e->next = p;
}

void
remove_odr_car (odr_parking_lot *t, odr_car *e)
{
    if (NULL == t)
    {
        return;
    }

    if (*t == e)
    {
        *t = e->next;
        destroy_odr_car(e);
        return;
    }

    odr_car *p, *prev;
    for (prev = *t, p = prev->next; NULL != p; p = p->next)
    {
        if (p == e)
        {
            prev->next = p->next;
            destroy_odr_car(e);
            return;
        }
        prev = p;
    }
}

void
park_msg (routing_table *t, msg_iovec msg)
{
    msg_header *header = msg[0].iov_base;
    routing_table_entry *rte = find_routing_table_entry(*t, header->destination_id);
    
    if (NULL == rte)
    {
        rte = create_routing_table_entry();
        insert_in_routing_table(t, rte);
        memcpy(rte->destination, header->destination_id, INET_ADDRSTRLEN);
    }
    insert_in_odr_parking_lot(&(rte->parking_lot), create_odr_car(msg));
}
