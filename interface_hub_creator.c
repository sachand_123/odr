#include "app_settings.h"

#define LOG_TAG "HubCreator"

/**
 * Creates the interface hub.
 * 
 * This is where the code records and binds with appropriate interfaces.
 * */
endpoint_list
create_listen_hub ()
{
    endpoint_list if_list = get_endpoint_list();
    endpoint *t = if_list;

    while (NULL != t)
    {
        SOCKET sock = bind_socket(t->index);
        if (SOCKET_ERROR == sock)
        {
            LOGE("Couldn't be bound to following endpoint. Skipping:%s",
                    endpoint_to_string(t));
            endpoint *temp = t->next;
            remove_endpoint(&if_list, t);
            t = temp;
            continue;
        }

        t->sock = sock;
        LOGS("Binding socket on %s", endpoint_to_string(t));
        t = t->next;
    }
    printf("\n");

    return if_list;
}
