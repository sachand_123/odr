#include "app_settings.h"

#define LOG_TAG "Dummy"

int main()
{
    int cnt = 1;
    char *g_my_file_name = calloc(1, 90);
    memcpy(g_my_file_name, SERVER_IPC_ABSFILENAME, strlen(SERVER_IPC_ABSFILENAME));
    SOCKET sock = bind_ipc(g_my_file_name);
    
    LOGS("sock %d %s", sock, errno_string());
    errno = 0;
    
    connect(sock, get_sun(ODR_IPC_ABSFILENAME), sizeof(struct sockaddr_un));
    char sip[20] = { '\0' };
    uint16_t sp = 0;
    char buf[512];
    int len = 512;
    while (cnt < 100)
    {
        if (0 < recv_msg(sock, sip, &sp, buf, &len, 3000))
        {
            LOGS("sip %s sp %u dat %s", sip, sp, buf);
        }
        ++cnt;
    }
    LOGS("sock %d %s", sock, errno_string());
    
    close(sock);
    remove(g_my_file_name);
    free(g_my_file_name);
    return 0;
}
