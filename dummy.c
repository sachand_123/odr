#include "app_settings.h"

#define LOG_TAG "Dummy"

int main()
{
    char *g_my_file_name = calloc(1, 90);
    SOCKET sock = bind_ipc(g_my_file_name);
    
    LOGS("sock %d %s", sock, errno_string());
    errno = 0;
    
    connect(sock, get_sun(ODR_IPC_ABSFILENAME), sizeof(struct sockaddr_un));
    send_msg(sock, ME, DEFAULT_PORT_NUMBER, "sad", 3, FALSE);
    LOGS("sock %d %s", sock, errno_string());
    
    close(sock);
    remove(g_my_file_name);
    return 0;
}
