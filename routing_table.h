#pragma once

#include "extras.h"
#include "odr_parking_lot.h"

#define HW_ADDRLEN (6)

typedef uint8_t interface;

typedef struct relaying_history_t
{
    uint32_t *history;
    int history_size;
    int next_slot;
} relaying_history;

typedef struct routing_table_entry_t
{
    char        destination[INET_ADDRSTRLEN];
    uint8_t     next_hop[HW_ADDRLEN];
    interface   outgoing_if;
    int         hop_count;
    
    relaying_history relaying_broadcasts;
    boolean finding_route;
    long timestamp;
    odr_parking_lot parking_lot;
    
    struct routing_table_entry_t *next;    
} routing_table_entry;

typedef routing_table_entry *routing_table;

void
destroy_routing_table (routing_table t);

routing_table_entry *
create_routing_table_entry ();

void
destroy_routing_table_entry (routing_table_entry *e);

routing_table_entry *
find_routing_table_entry (routing_table t, char *destination);

void
insert_in_routing_table (routing_table *t, routing_table_entry *e);

void
remove_routing_table_entry (routing_table *t, routing_table_entry *e);

boolean
is_valid_routing_table_entry (routing_table_entry *e);
