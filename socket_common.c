#include "app_settings.h"

#define LOG_TAG "SockCommon"

inline int
set_socket_broadcast (SOCKET sock)
{
	int optval = 1;
	return setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char *) &optval,
			sizeof(optval));
}

int
ready_socket_count (SOCKET sock, uint32_t ms)
{
    struct timeval timeout = ms_to_timeval(ms);

    fd_set rdfds;
    FD_ZERO(&rdfds);
    FD_SET(sock, &rdfds);

    return select(sock + 1, &rdfds, NULL, NULL, &timeout);
}

int
wait_for_ready_socket(SOCKET sock)
{
    fd_set rdfds;
    FD_ZERO(&rdfds);
    FD_SET(sock, &rdfds);

    return select(sock + 1, &rdfds, NULL, NULL, NULL);
}

int
wait_for_ready_sockets(endpoint_list list, fd_set *rdfds)
{
    FD_ZERO(rdfds);
    int max_fd = 0;

    endpoint *temp;
    for (temp = list; NULL != temp; temp = temp->next)
    {
        FD_SET(temp->sock, rdfds);
        max_fd = max_fd < temp->sock ? temp->sock : max_fd;
    }

    struct timeval timeout = ms_to_timeval(5000);
    return select(max_fd + 1, rdfds, NULL, NULL, &timeout);
}

void
unconnect_socket (SOCKET sock)
{
    struct sockaddr_in sin_unconnect;
    memset(&sin_unconnect, 0, sizeof(struct sockaddr_in));
    sin_unconnect.sin_family = AF_UNSPEC;
    
    connect(sock, &sin_unconnect, sizeof(struct sockaddr_in));
}

struct sockaddr_un*
get_sun (char *file_name)
{
    struct sockaddr_un *sun = calloc(1, sizeof(struct sockaddr_un));
    if (NULL != sun)
    {
        sun->sun_family = AF_LOCAL;
        memcpy(sun->sun_path, file_name, strlen(file_name));
    }

    return sun;
}

/**
 * Floods given msg_iovec on the L2 n/w.
 * 
 * @param list      Endpoint list with all the interfaces
 * @param p         Endpoint from which broadcast is NOT to be sent.
 *                  Specify NULL if pure broadcast desired
 * @param msg       Message to flood
 * 
 * @return Number of interfaces on which broadcasted
 * 
 * @todo check.... might not be right.
 * */
int
socket_l2_flood_msg (endpoint_list list, endpoint *p, msg_iovec msg)
{
    int bytes_sent = 0;
    
    endpoint *t;
    for (t = list; NULL != t; t = t->next)
    {
        if (NULL != p && t->index == p->index) { continue; }

        bytes_sent += (0 < socket_l2_send_unicast(t, msg, NULL)) ? 1 : 0;
    }
}

/**
 * Sends a message on an L2 socket
 * 
 * @param p         Endpoint from which the msg has to leave
 * @param msg       What to send
 * @param destination_hardware_address If NULL, broadcast is assumed
 * 
 * @return Number of bytes sent
 * */
int
socket_l2_send_unicast (endpoint *p, msg_iovec msg, uint8_t *destination_hardware_address)
{
    if (NULL == p || NULL == msg) { return 0; }

    struct sockaddr_ll sll;
    memset(&sll, 0, sizeof(struct sockaddr_ll));
    sll.sll_family = AF_PACKET;
    sll.sll_protocol = htons(ETH_P_ODR);
    sll.sll_ifindex = p->index;
    sll.sll_halen = HW_ADDRLEN;
    if (NULL != destination_hardware_address)
    {
        memcpy(sll.sll_addr, p->hardware_address, HW_ADDRLEN);
    }
    else
    {
        memset(sll.sll_halen, 0xFF, HW_ADDRLEN);
    }

    struct iovec msg_iov[3];
    ethernet_frame_header *eth_header = calloc(1, sizeof(ethernet_frame_header));
    if (NULL != destination_hardware_address)
    {
        memcpy(eth_header->destination_hardware_address, destination_hardware_address, HW_ADDRLEN);
    }
    else
    {
        memset(eth_header->destination_hardware_address, 0xFF, HW_ADDRLEN);
    }
    memcpy(eth_header->source_hardware_address, p->hardware_address, HW_ADDRLEN);
    eth_header->ethertype = htons(ETH_P_ODR);
    msg_iov[0].iov_base = eth_header;
    msg_iov[0].iov_len = sizeof(ethernet_frame_header);
    memcpy(&(msg_iov[1]), &(msg[0]), sizeof(struct iovec));
    memcpy(&(msg_iov[2]), &(msg[1]), sizeof(struct iovec));

    struct msghdr msg_msghdr;
    memset(&msg_msghdr, 0, sizeof(struct msghdr));
    msg_msghdr.msg_name = &sll;
    msg_msghdr.msg_namelen = sizeof(struct sockaddr_ll);
    msg_msghdr.msg_iov = msg_iov;
    msg_msghdr.msg_iovlen = 3;

    int ret = sendmsg(p->sock, &msg_msghdr, MSG_DONTWAIT);
    free (eth_header);
    return ret;
}

/**
 * Core method that write on the socket. This function is non-blocking.
 * 
 * @param sock          Socket fd to write to
 * @param msg           Message to write
 * 
 * @return On success, this call returns the number of characters sent.
 * On error, -1 is returned, and errno is set appropriately.
 * */
uint32_t
socket_send_msg_default (SOCKET sock, msg_iovec msg)
{
    if (NULL == msg) { return 0; }

    struct msghdr msg_msghdr;
    memset(&msg_msghdr, 0, sizeof(struct msghdr));
    msg_msghdr.msg_iov = msg;
    msg_msghdr.msg_iovlen = 2;
    msg_header *h = msg[0].iov_base;

    return HEADER_SIZE + h->payload_length;
}

/**
 * Core method that reads from the socket.
 * 
 * @param sock          Socket fd to read from
 * @param msg           Pointer to message this function fills
 * @param timeout_ms    Time to wait for read
 * 
 * @return If positive, it tells the number of  bytes read.
 * If 0 is returned then it implies nothing to read within the tineout
 * If SOCKET_ERROR is returned, it implies an error. Caller should read
 * errno
 * */
int
socket_recv_msg_full (SOCKET sock, struct msghdr *msg_msghdr, int timeout_ms)
{
    msg_iovec r_msg = create_msg_max();
    msg_msghdr->msg_iov = r_msg;
    msg_msghdr->msg_iovlen = 2;
    msg_msghdr->msg_namelen = sizeof(struct sockaddr_un);
    msg_msghdr->msg_name = calloc(1, sizeof(struct sockaddr_un));

    int bytes_read = 0;
    LOGV("Socket: %d", sock);
    int rsc = ready_socket_count(sock, timeout_ms);
    if (1 == rsc)
    {
        bytes_read = recvmsg(sock, msg_msghdr, MSG_DONTWAIT);
        if (0 > bytes_read)
        {
            LOGE("Recv failed even after select succeeded. Error: %s",
                    errno_string());
        }
        else if (0 < bytes_read)
        {
            print_msg_header(r_msg[0].iov_base, "RECV");
        }
    }
    else if (0 > rsc)
    {
        bytes_read = SOCKET_ERROR;
    }  
    else
    {
        errno = ETIMEDOUT;
        bytes_read = 0;
    }

    if (0 >= bytes_read)
    {
        delete_msg(r_msg);
    }

    return bytes_read;
}

/**
 * Core method that reads from the socket.
 * 
 * @param sock          Socket fd to read from
 * @param msg           Pointer to message this function fills
 * @param timeout_ms    Time to wait for read
 * 
 * @return If positive, it tells the number of  bytes read.
 * If 0 is returned then it implies nothing to read within the tineout
 * If SOCKET_ERROR is returned, it implies an error. Caller should read
 * errno
 * */
uint32_t
socket_recv_msg_default (SOCKET sock, msg_iovec *msg, int timeout_ms)
{
    struct msghdr msg_msghdr;
    memset(&msg_msghdr, 0, sizeof(struct msghdr));

    int ret = socket_recv_msg_full(sock, &msg_msghdr, timeout_ms);
    if (0 < ret)
    {
        *msg = msg_msghdr.msg_iov;
    }

    return ret;
}
