#include "app_settings.h"

#define LOG_TAG "ODR_Client"

static uint16_t g_next_ephemeral_port = DEFAULT_PORT_NUMBER + 1;

void
destroy_odr_client (odr_client *e)
{
    free(e);
}

void
destroy_odr_client_list (odr_client_list l)
{
    odr_client *e, *s;
    for (e = l; NULL != e; e = s)
    {
        s = e->next;
        destroy_odr_client(e);
    }
}

odr_client *
create_odr_client (char *file_name, uint16_t port)
{
    odr_client *t = (odr_client *) calloc(1, sizeof(odr_client));
    strncpy(t->file_name, file_name, ODR_CLIENT_RAND_MAX_FILENAME);
    t->port = port;
}

odr_client *
find_odr_client (odr_client_list t, uint16_t port)
{
    odr_client *e;
    for (e = t; NULL != e; e = e->next)
    {
        if (port == e->port)
        {
            break;
        }
    }

    return e;
}

void
insert_in_odr_client_list (odr_client_list *t, odr_client *e)
{
    odr_client *p = find_odr_client(*t, e->port);
    if (NULL == p)
    {
        p = *t;
        *t = e;
        e->next = p;
    }
    else
    {
        strncpy(p->file_name, e->file_name, ODR_CLIENT_RAND_MAX_FILENAME);
    }
}

uint16_t
get_next_ephemeral_port ()
{
    uint16_t ret = g_next_ephemeral_port;
    if ((short)g_next_ephemeral_port >= SHRT_MAX)
    {
        g_next_ephemeral_port = DEFAULT_PORT_NUMBER + 1;
    }
    else
    {
        ++g_next_ephemeral_port;
    }

    return ret;
}

uint16_t
register_odr_client (odr_client_list *list, char *file_name, uint16_t port)
{
    if (0 == port)
    {
        port = get_next_ephemeral_port();
    }

    odr_client *oc = find_odr_client(*list, port);
    if (NULL == oc)
    {
        oc = create_odr_client(file_name, port);
        insert_in_odr_client_list(list, oc);
    }
    else
    {
        strncpy(oc->file_name, file_name, ODR_CLIENT_RAND_MAX_FILENAME);
    }

    return port;
}

void
remove_odr_client (odr_client_list *t, odr_client *e)
{
    if (NULL == t)
    {
        return;
    }

    if (*t == e)
    {
        *t = e->next;
        destroy_odr_client(e);
        return;
    }

    odr_client *p, *prev;
    for (prev = *t, p = prev->next; NULL != p; p = p->next)
    {
        if (p == e)
        {
            prev->next = p->next;
            destroy_odr_client(e);
            return;
        }
        prev = p;
    }
}
