#pragma once

#include <pthread.h>
#include <semaphore.h>
#include <limits.h>
#include <netpacket/packet.h>
#include <sys/un.h>

#include "socket_common.h"
#include "endpoint.h"
#include "logger.h"
#include "extras.h"
#include "np_time.h"
#include "routing_table.h"
#include "msg_header.h"
#include "ethernet_frame.h"
#include "odr_client.h"
#include "odr_parking_lot.h"
#include "odr_router.h"

#define ETH_P_ODR (0x4969)

#define MKSTEMP_TEMPLATE "/tmp/schandXXXXXX"
#define MKSTEMP_TEMPLATE_LENGTH 20

#define ODR_IPC_ABSFILENAME "/tmp/ODR109954969"
#define ODR_IPC_READ_TIMEOUT_MS 5000

#define SERVER_IPC_ABSFILENAME "/tmp/SVR109954969\0"

/**
 * Protocol flags
 * */
#define MSG_HEADER_FLAG_REQ (1 << 0)
#define MSG_HEADER_FLAG_REP (1 << 1)
#define MSG_HEADER_FLAG_DAT (1 << 2)
#define MSG_HEADER_FLAG_FRC (1 << 3)
#define MSG_HEADER_FLAG_ERR (1 << 4)

#define MAX_MESSAGE_SIZE 512
#define MAX_PAYLOAD_SIZE (MAX_MESSAGE_SIZE - HEADER_SIZE)

/**
 * Server default port number.
 * Used if *.in not found
 * */
#define DEFAULT_PORT_NUMBER 47991
