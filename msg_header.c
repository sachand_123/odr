#include "app_settings.h"

#define LOG_TAG "MsgHeader"

msg_header *
get_new_header ()
{
    return (msg_header *) calloc(1, sizeof(msg_header));
}

char *
msg_header_flags_to_string (uint16_t flags)
{
    static char flag_string[80];
    char *temp = flag_string;
    memset(temp, 0, 80);

    temp += sprintf(temp, "%s", "_");
    
    if (0 != (flags & MSG_HEADER_FLAG_REQ))
    {
        temp += sprintf(temp, "%s", "REQ_");
    }
    if (0 != (flags & MSG_HEADER_FLAG_REP))
    {
        temp += sprintf(temp, "%s", "REP_");
    }
    if (0 != (flags & MSG_HEADER_FLAG_DAT))
    {
        temp += sprintf(temp, "%s", "DAT_");
    }
    if (0 != (flags & MSG_HEADER_FLAG_FRC))
    {
        temp += sprintf(temp, "%s", "FRC_");
    }
    if (0 != (flags & MSG_HEADER_FLAG_ERR))
    {
        temp += sprintf(temp, "%s", "ERR_");
    }

    return flag_string;
}

void
print_msg_header (msg_header *header, char *prefix)
{
    LOGS("%s Dest=%s:%u Flags=%s Len=%u", prefix, header->destination_id,
            header->destination_port, msg_header_flags_to_string(header->flags),
            header->payload_length);
}

/**
 * Swaps desitnation and source id and port in the header
 * */
void
swap_roles (msg_header *header)
{
    char temp_net[INET_ADDRSTRLEN];
    uint16_t temp_port;
    
    memcpy(temp_net, header->source_id, INET_ADDRSTRLEN);
    temp_port = header->source_port;
    memcpy(header->source_id, header->destination_id, INET_ADDRSTRLEN);
    header->source_port = header->destination_port;
    memcpy(header->destination_id, temp_net, INET_ADDRSTRLEN);
    header->destination_port = temp_port;
}
