CC = gcc

CFLAGS = -w -g -I/home/schand/Downloads/unpv13e/lib

RM = rm -f

SYSTEM_LIBS = \
	-lpthread \
	-lm \
	-lrt \
	libunp.a\

TARGET_LIBS = \
	odr_router \
	client \
	server \

#The original utils
SOURCES_UTILS = \
	logger.c \
	np_time.c \
	extras.c \

#Shared files with headers
SOURCES_COMMON = \
	${SOURCES_UTILS} \
	endpoint.c \
	msg_header.c \
	socket_common.c \

HEADERS_COMMON = $(SOURCES_COMMON:.c=.h)

#Shared file but no headers
SOURCES_BASE = \
	${SOURCES_COMMON} \
	get_hw_addrs.c \
	interface_hub_creator.c \
	socket_binder.c \
	channel_wrapper.c \
	ipc_provider.c \

#ODR's files
SOURCES_ODR_ROUTER = \
	${SOURCES_BASE} \
	odr_router.c \
	ethernet_frame_processor.c \
	odr_client.c \
	odr_parking_lot.c \
	router_top_backend.c \
	router_top_frontend.c \
	routing_table.c \
	routing_table_provider.c \

#Client's files
SOURCES_CLIENT = \
	${SOURCES_BASE} \
	dummy.c \

#Server's files
SOURCES_SERVER = \
	${SOURCES_BASE} \
	dummys.c \

.PHONY: all
all: ${TARGET_LIBS} clean rename

server : server.o
	${CC} ${CFLAGS} -o server ${SOURCES_SERVER:.c=.o} ${SYSTEM_LIBS}
server.o : ${SOURCES_SERVER}
	${CC} ${CFLAGS} -c ${SOURCES_SERVER}

client : client.o
	${CC} ${CFLAGS} -o client ${SOURCES_CLIENT:.c=.o} ${SYSTEM_LIBS}
client.o : ${SOURCES_CLIENT}
	${CC} ${CFLAGS} -c ${SOURCES_CLIENT}

odr_router : odr_router.o
	${CC} ${CFLAGS} -o odr_router ${SOURCES_ODR_ROUTER:.c=.o} ${SYSTEM_LIBS}
odr_router.o : odr_router.c
	${CC} ${CFLAGS} -c ${SOURCES_ODR_ROUTER}

.PHONY: clean
clean:
	${RM} *.o

.PHONY: rename
rename:
	mv odr_router odr_109954969

